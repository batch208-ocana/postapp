package com.b208.postApp.controllers;

import com.b208.postApp.models.Post;
import com.b208.postApp.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

//Handle all HTTP Responses
@RestController
//Enables cross-origin resource request
//CORS - cross-origin resource sharing is the ability to allow or disallow applications to share resources from each other. With this, we can allow or disallow clients to access our API
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;

    //map web requests to controller methods via @RequestMapping
    //@RequestMapping(value="/posts", method = RequestMethod.POST)
    //@PostMapping maps post method requests into an endpoint.
    @PostMapping("/posts")
    //ResponseEntity is an object that represents and contains the whole HTTP response, the actual message, the status code.
    //@RequestHeader(value="Authorization") allows us to get the value passed as Authorization from our request.
    public ResponseEntity<Object> createPost(@RequestHeader(value="Authorization") String stringToken, @RequestBody Post post){
        //@RequestBody automatically converts JSON client input into our desired object.
        //user the createPost() method from our service and pass our post object.
        // if(post.getTitle().isEmpty() || post.getContent().isEmpty()){
        //     return new ResponseEntity<>("Invalid Input. Please complete the form", HttpStatus.OK);
        // } else {
        //     postService.createPost(post);
        //     return new ResponseEntity<>("Post Created Successfully", HttpStatus.CREATED);
        // }
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post Created Successfully", HttpStatus.OK);
        /*
            * RequestBody(JSON) -> Post post object in the controller -> postService.createPost(post) -> postRepository.save(post)
            * 
        */
    }

    @GetMapping("/posts")
    public ResponseEntity<Object> getPosts(){
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    @PutMapping("/posts/{id}")
    public ResponseEntity<Object> updatePost(@RequestHeader(value = "Authorization") String stringToken,@PathVariable Long id, @RequestBody Post post){
        // if(post.getTitle().isEmpty() || post.getContent().isEmpty()){
        //     return new ResponseEntity<>("Invalid Input. Please complete the form", HttpStatus.OK);
        // } else {
        //     postService.updatePost(id, post);

        //     return new ResponseEntity<>("Post updated successfully.", HttpStatus.OK);
        // }
        return postService.updatePost(stringToken, id, post);
    }

    @DeleteMapping("/posts/{id}")
    public ResponseEntity<Object> deletePost(@RequestHeader(value = "Authorization") String stringToken,@PathVariable Long id){
        return postService.deletePost(stringToken,id);
    }

    @GetMapping("/myPosts")
    public ResponseEntity<Object> getUserPosts(@RequestHeader(value = "Authorization") String stringToken){
        return postService.getUserPosts(stringToken);
    }
}