package com.b208.postApp.controllers;

import com.b208.postApp.exceptions.UserException;
import com.b208.postApp.models.User;
import com.b208.postApp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
public class UserController {
    @Autowired
    UserService userService;

    @PostMapping("/users/register")
    public ResponseEntity<Object> register(@RequestBody Map<String, String> body) throws UserException {
        /*if (user.getPassword().length() >= 8) {
            userService.createUser(user);
            return new ResponseEntity<>("New User Created Successfully.", HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>("Please add an 8 or more character password.",HttpStatus.OK);
        }*/
        String username = body.get("username");

        if(!userService.findByUsername(username).isEmpty()){
            throw new UserException("Username already exist");
        } else {
            String password = body.get("password");
            String encodedPassword = new BCryptPasswordEncoder().encode(password);

            User newUser = new User(username, encodedPassword);

            userService.createUser(newUser);
            return new ResponseEntity<>("User registered successfully",HttpStatus.CREATED);
        }
    }

    @GetMapping("/users")
    public ResponseEntity<Object> getUsers(){
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable Long id, @RequestBody User user){
        userService.updateUser(id, user);
        return new ResponseEntity<>("User updated successfully.",HttpStatus.OK);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable Long id){
        userService.deleteUser(id);
        return new ResponseEntity<>("User deleted successfully.",HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserDetails(@PathVariable Long id){
        return new ResponseEntity<>(userService.getUserInfo(id), HttpStatus.OK);
    }

    @GetMapping("/users/search/{id}")
    public ResponseEntity<Object> checkIfExist(@PathVariable Long id){
        return new ResponseEntity<>(userService.checkIfExist(id), HttpStatus.OK);
    }

    @GetMapping("/users/count")
    public ResponseEntity<Object> countUsers(){
        return  new ResponseEntity<>("Number of users: " +userService.countUsers(), HttpStatus.OK);
    }
}
