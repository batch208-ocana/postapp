package com.b208.postApp.services;

import com.b208.postApp.models.User;

import java.util.Optional;

public interface UserService {
    void createUser(User user);

    Iterable<User> getUsers();

    void updateUser(Long id, User user);

    void deleteUser(Long id);

    Optional<User> getUserInfo(Long id);

    String checkIfExist(Long id);

    Long countUsers();

    Optional<User> findByUsername(String username);
}
