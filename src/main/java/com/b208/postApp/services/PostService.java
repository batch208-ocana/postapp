package com.b208.postApp.services;

import org.springframework.http.ResponseEntity;

import com.b208.postApp.models.Post;

public interface PostService {
    void createPost(String stringToken, Post post);
    Iterable<Post> getPosts();
    ResponseEntity updatePost(String stringToken, Long id, Post post);
    ResponseEntity deletePost(String stringToken, Long id);

    ResponseEntity getUserPosts(String stringToken);
}
