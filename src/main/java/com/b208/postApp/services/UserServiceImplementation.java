package com.b208.postApp.services;

import com.b208.postApp.models.User;
import com.b208.postApp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImplementation implements UserService{

    @Autowired
    private UserRepository userRepository;

    public void createUser(User user){
        userRepository.save(user);
    }

    public Optional<User> findByUsername(String username){
        return Optional.ofNullable(userRepository.findByUsername(username));
    }

    public Iterable<User> getUsers(){
        return userRepository.findAll();
    }

    public void updateUser(Long id, User user){
        User userUpdate = userRepository.findById(id).get();
        userUpdate.setUsername(user.getUsername());
        userUpdate.setPassword(user.getPassword());
        userRepository.save(userUpdate);
    }

    public void deleteUser(Long id){
        userRepository.deleteById(id);
    }

    public Optional<User> getUserInfo(Long id){
        return userRepository.findById(id);
    }

    public String checkIfExist(Long id){
        if(userRepository.existsById(id)){
            return "User Found";
        } else {
            return "User Not Found";
        }
    }

    public  Long countUsers(){
        return userRepository.count();
    }
}
