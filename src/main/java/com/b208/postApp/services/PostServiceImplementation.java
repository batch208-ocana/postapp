package com.b208.postApp.services;

import com.b208.postApp.models.Post;
import com.b208.postApp.config.JwtToken;
import com.b208.postApp.models.User;
import com.b208.postApp.repositories.PostRepository;
import com.b208.postApp.repositories.UserRepository;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

//@Services is added so that springboot would recognize that business logic is implemented in this layer
@Service
public class PostServiceImplementation implements PostService{
    //Were going to create an instance of the postRepository that is persistent within our springboot app
    //This is, so we can use the methods for database manipulation for our table
    @Autowired
    private PostRepository postRepository;

    @Autowired
    JwtToken jwtToken;

    @Autowired
    private UserRepository userRepository;

    public void createPost(String stringToken, Post post){
        //Get the username from the token using jwtToken getUsernameFromToken method.
        //Then, pass the username:
        //Get the user's details from our table, using our userRepository findByUsername method.
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Post newPost = new Post();
        newPost.setTitle(post.getContent());
        newPost.setContent(post.getContent());
        newPost.setUser(author);

        //when createPost is used from our services, we will be able to pass a Post class object and then, using the .save() method of our postRepository we will be able to insert a new row into our posts table.
        postRepository.save(newPost);
    }

    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }

    public ResponseEntity updatePost(String stringToken, Long id, Post post){
        //post parameter here actually the request object passed from the controller
        //because post parameter is actually the request body converted as a post class object, it has the methods from a post class object.
        //Therefore, getTitle() will actually get the value of the title property from the request body(JSON)
        //findById() retrieves a record that matches the passed it
        Post postForUpdating = postRepository.findById(id).get();
        String postAuthor = postForUpdating.getUser().getUsername();

        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUser.equals(postAuthor)){
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);

            return new ResponseEntity<>("Post Updated Successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to update this post.", HttpStatus.UNAUTHORIZED);
        }
    }

    public ResponseEntity deletePost(String stringToken, Long id){

        Post postForDeleting = postRepository.findById(id).get();

        String postAuthor = postForDeleting.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUser.equals(postAuthor)){
            postRepository.deleteById(id);
            return new ResponseEntity<>("Post Deleted Successfully",HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to delete this post", HttpStatus.UNAUTHORIZED);
        }
    }

    public ResponseEntity getUserPosts(String stringToken){
        
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        Iterable<Post> getPosts = postRepository.findAll();
        ArrayList<Post> postByUser = new ArrayList<Post>();


        for (Post post : getPosts) {
            if(authenticatedUser.equals(post.getUser().getUsername())){
                postByUser.add(post);
            } 
        }
        return new ResponseEntity<>(postByUser,HttpStatus.OK);
    }
}
